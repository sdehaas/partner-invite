module.exports = {
    "extends": "airbnb-base",
    "rules": {
        "linebreak-style": 0,
        "quotes": ["error", "single", { "allowTemplateLiterals": true }],
        "max-len": ["error", 120],
        "no-param-reassign": 0,
        "no-console": 0,
        "prefer-promise-reject-errors": 0,
        "comma-dangle": 0,
        "no-use-before-define": 0,
        "no-useless-escape": 0,
        "no-await-in-loop": 0,
        "no-restricted-syntax": 0,
        "consistent-return": 0,
        "no-underscore-dangle": 0
    }
};
