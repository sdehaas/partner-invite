class Retry {
  /**
   * Retry wrapper for every generic function that is async (promise based)
   * @param fn {Function}
   * @param [fnArgs] {Array<any>}
   * @param [maxRetries] {Number}
   * @param [retries] {Number}
   * @returns {Promise<*>}
   */
  static async retryWrapper(fn, fnArgs = [], maxRetries = 3, retries = 0) {
    try {
      return await fn.call(this, ...fnArgs);
    } catch (err) {
      if (retries >= maxRetries) {
        return Promise.reject(`Max retries ${maxRetries} exceeded. Function error: ${err}`);
      }
      retries += 1;
      return Retry.retryWrapper(fn, fnArgs, maxRetries, retries);
    }
  }
}

module.exports = Retry;
