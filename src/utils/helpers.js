const Path = require('path');
const { existsSync } = require('fs');

class Helpers {
  /**
   * Get application arguments from argv
   * @param appArguments {Array}
   * @returns {{maxDistance: number, inputFilePath: string, lon: number, lat: number}}
   */
  static loadAppArguments(appArguments) {
    try {
      const {
        maxDistance, inputFilePath, lon, lat
      } = Helpers.readAppParameters(appArguments);
      if (!maxDistance || !inputFilePath || !lon || !lat) {
        Helpers.showUsage();
        throw new Error('Missing required arguments');
      }
      Helpers.ensureValidMaxDistance(maxDistance);
      Helpers.ensureValidFilePath(inputFilePath);
      Helpers.ensureValidLon(lon);
      Helpers.ensureValidLat(lat);
      return {
        maxDistance, inputFilePath, lon, lat,
      };
    } catch (validateErr) {
      throw new Error(validateErr);
    }
  }

  /**
   * Get the values for the given app arguments
   * @param appArguments {Array}
   * @returns {{maxDistance: number, inputFilePath: string, lon: number, lat: number}}
   */
  static readAppParameters(appArguments) {
    let maxDistance = null;
    let inputFilePath = null;
    let lon = null;
    let lat = null;
    for (const param of appArguments) {
      const split = param.split('=');
      const key = split[0];
      const value = split[1];
      if (value) {
        switch (key) {
          case '--maxDistance':
            maxDistance = parseInt(value, 0);
            break;
          case '--inputFile':
            inputFilePath = Path.resolve(value);
            break;
          case '--lon':
            lon = parseFloat(value);
            break;
          case '--lat':
            lat = parseFloat(value);
            break;
          default:
            break;
        }
      }
    }

    return {
      maxDistance,
      inputFilePath,
      lon,
      lat,
    };
  }

  /**
   * Make sure the given value for --maxDistance is correct
   * @param maxDistance {Number}
   */
  static ensureValidMaxDistance(maxDistance) {
    if (!maxDistance) {
      throw new Error('--maxDistance is required');
    }
    if (Number.isNaN(maxDistance)) {
      throw new Error(`--maxDistance (${maxDistance}) has an invalid number`);
    }
  }

  /**
   * Make sure the given value for --lon is correct
   * @param lon {Number}
   */
  static ensureValidLon(lon) {
    if (!lon) {
      throw new Error('--lon is required and to be a number');
    }
    if (typeof lon !== 'number') {
      throw new Error(`--lon (${lon}) is not a number`);
    }
  }

  /**
   * Make sure the given value for --lat is correct
   * @param lat {Number}
   */
  static ensureValidLat(lat) {
    if (!lat) {
      throw new Error('--lat is required and to be a number');
    }
    if (typeof lat !== 'number') {
      throw new Error(`--lat (${lat}) is not a number`);
    }
  }

  /**
   * Make sure the given value for --filePath is correct
   * @param filePath {String}
   */
  static ensureValidFilePath(filePath) {
    if (!filePath) {
      throw new Error('--inputFile is required');
    }

    const fileExtension = filePath.split('.').pop();
    if (fileExtension.toLowerCase() !== 'json') {
      throw new Error('--inputFile needs to point to .json file');
    }

    if (!existsSync(filePath)) {
      throw new Error(`--inputFile (${filePath}) does not exists.`);
    }
  }

  static showUsage() {
    console.log(`Required parameters:
  --maxDistance={} integer that represents a kilometer value (KM)
  --inputFile={} string that represents path to your input file (JSON)
  --lon={} float that represents lon value of office location
  --lat={} float that represents lat  value of office location
  Example: --maxDistance=30 --lat=51.839549 --lon=5.87837 --inputFile=./partners.json`);
  }
}

module.exports = Helpers;
