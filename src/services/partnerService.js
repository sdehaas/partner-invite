const geoLocationService = require('./geoLocationService');
const greatCircleDistanceService = require('./greatCircleDistanceService');
const retry = require('../utils/retry');

class PartnerService {
  /**
   * Complete partner information, which includes adding the distance from the partner's location to the given
   * office location.
   * @param partners {Array<Object>}
   * @param officeLon {Number}
   * @param officeLat {Number}
   * @returns {Promise<Array<Object>>}
   */
  static completePartnerInfo(partners, officeLon, officeLat) {
    if (!partners || !Array.isArray(partners)) {
      throw new Error('No array passed to completePartnerInfo function');
    }
    if (!officeLon || !officeLat) {
      throw new Error('Lon/Lat params are mandatory for completePartnerInfo function');
    }
    const promises = partners.map(partner => PartnerService.completeDataForPartner(partner, officeLon, officeLat));
    return Promise.all(promises);
  }

  /**
   * This function completes the data of 1 partner. It gets the coordinates of the partner's address and calculates
   * the distance between the two locations.
   * @param partner {Object}
   * @param officeLon {Number}
   * @param officeLat {Number}
   * @returns {Promise<Object>}
   */
  static async completeDataForPartner(partner, officeLon, officeLat) {
    if (!partner.address) {
      throw new Error(`Partner ${JSON.stringify(partner)} is missing address Object`);
    }
    try {
      const { lon, lat } = await retry.retryWrapper(geoLocationService.getCoordinatesForAddress, [
        partner.address.street,
        partner.address.no,
        partner.address.city,
        partner.address.country,
      ]);
      const distance = greatCircleDistanceService.calculateGreatCircleDistance(officeLon, officeLat, lon, lat);
      return {
        ...partner,
        address: {
          ...partner.address,
          lon,
          lat,
        },
        distance,
      };
    } catch (coordinateErr) {
      throw new Error(coordinateErr);
    }
  }

  /**
   * Filter partner array based on distance between the two locations provided by a max distance
   * @param partners {Array<Object>}
   * @param maxDistance {Number}
   * @returns {Array<Object>}
   */
  static filterPartnersBasedOnMaxDistance(partners, maxDistance) {
    return partners.filter(partner => partner && partner.distance <= maxDistance);
  }

  /**
   * Sort partners by their name attribute
   * @param partners {Array<Object>}
   * @returns {Array<Object>}
   */
  static sortPartnersByName(partners) {
    return partners.sort(({ name: nameA }, { name: nameB }) => {
      if (nameA === nameB) return 0;
      if (nameA < nameB) return -1;
      if (nameA === null) return 1;
      if (nameB === null) return -1;
      return 1;
    });
  }
}

module.exports = PartnerService;
