const request = require('request');
const { OPEN_STREETS_API, USER_AGENT } = require('../config');

class GeoLocationService {
  /**
   * Get lon/lat for given address
   * @param streetName {String}
   * @param streetNumber {String}
   * @param city {String}
   * @param country {String}
   * @returns {Promise<Object>}
   */
  static getCoordinatesForAddress(streetName, streetNumber, city, country) {
    return new Promise(async (resolve, reject) => {
      if (!streetName || !streetNumber || !city || !country) {
        reject(`Incomplete data: Street: ${streetName}, StreetNumber: ${streetNumber}, City: ${city}`
            + ` Country: ${country}`);
      }
      const requestOptions = {
        method: 'GET',
        uri: OPEN_STREETS_API,
        headers: {
          'User-Agent': USER_AGENT,
        },
        qs: {
          street: `${streetNumber} ${streetName}`,
          city,
          country,
          format: 'json',
          limit: 1,
        },
      };
      request(requestOptions, (err, response, body) => {
        if (err) {
          reject(err);
        } else if (response.statusCode === 200) {
          try {
            const result = JSON.parse(body);
            resolve(GeoLocationService.getLonLatFromResult(result));
          } catch (lonLatErr) {
            reject(`Error while processing ${streetName} ${streetNumber} ${city} ${country}. Error: ${lonLatErr}`);
          }
        } else {
          reject(`Request for ${streetName} ${streetNumber} ${city} ${country} `
              + `failed with statusCode: ${response.statusCode}`);
        }
      });
    });
  }

  /**
   * Get lon/lat from response object
   * @param result {Array}
   * @returns {{lon, lat,}}
   */
  static getLonLatFromResult(result) {
    if (!result || !result[0]) {
      throw new Error(`No result found`);
    }
    const { lon, lat } = result[0];
    if (!lon || !lat) {
      throw new Error(`No lon/lat found in result`);
    }
    return { lon: parseFloat(lon), lat: parseFloat(lat) };
  }
}

module.exports = GeoLocationService;
