class GreatCircleDistanceService {
  /**
   * Calculate great circle distance for two coordinates in KM. Result is 1 decimal number.
   * @param lonA {Number}
   * @param latA {Number}
   * @param lonB {Number}
   * @param latB {Number}
   * @returns {number}
   */
  static calculateGreatCircleDistance(lonA, latA, lonB, latB) {
    const r = 6371.009; // Radius for KM
    latA *= Math.PI / 180;
    lonA *= Math.PI / 180;
    latB *= Math.PI / 180;
    lonB *= Math.PI / 180;
    const lonDelta = lonB - lonA;
    const a = Math.pow(Math.cos(latB) * Math.sin(lonDelta), 2) + Math.pow(Math.cos(latA) //eslint-disable-line
        * Math.sin(latB) - Math.sin(latA) * Math.cos(latB) * Math.cos(lonDelta), 2);  //eslint-disable-line
    const b = Math.sin(latA) * Math.sin(latB) + Math.cos(latA) * Math.cos(latB) * Math.cos(lonDelta); //eslint-disable-line
    const angle = Math.atan2(Math.sqrt(a), b);
    return parseFloat((angle * r).toFixed(1));
  }
}

module.exports = GreatCircleDistanceService;
