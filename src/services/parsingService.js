const { readFile } = require('fs');

class ParsingService {
  /**
   * Load json from a given file
   * @param filePath {String}
   * @returns {Promise<Object>}
   */
  static loadJSONFromInputFile(filePath) {
    return new Promise((resolve, reject) => {
      readFile(filePath, 'utf-8', (err, data) => {
        if (err) {
          reject(err);
        } else {
          try {
            resolve(ParsingService.parseDataAsJson(data));
          } catch (parseErr) {
            reject(parseErr);
          }
        }
      });
    });
  }

  /**
   * Parse data as JSON
   * @param data {String}
   * @returns {Object}
   */
  static parseDataAsJson(data) {
    try {
      return JSON.parse(data);
    } catch (parseErr) {
      throw new Error(`Error while parsing data from file. Error: ${parseErr}`);
    }
  }
}

module.exports = ParsingService;
