const helpers = require('./utils/helpers');
const parsingService = require('./services/parsingService');
const partnerService = require('./services/partnerService');

const APP_ARGUMENTS = process.argv;

async function main() {
  if (!process.env.test) {
    const partners = await getPartnersToSendInviteTo();
    console.log(JSON.stringify(partners, null, 3));
  }
}

async function getPartnersToSendInviteTo(appArguments) {
  try {
    const {
      maxDistance, inputFilePath, lon, lat,
    } = helpers.loadAppArguments(appArguments || APP_ARGUMENTS);
    const parsedFileData = await parsingService.loadJSONFromInputFile(inputFilePath);
    const updatedPartners = await partnerService.completePartnerInfo(parsedFileData, lon, lat);
    const filteredPartners = partnerService.filterPartnersBasedOnMaxDistance(updatedPartners, maxDistance);
    return partnerService.sortPartnersByName(filteredPartners);
  } catch (err) {
    console.log(err);
  }
  return null;
}

main();

module.exports = { getPartnersToSendInviteTo };
