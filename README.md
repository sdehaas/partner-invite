# Partner-Invite

Find the distance (great circle distance) between you and your partners based on the address of your partners. Filter the final list on your maxDistance argument.

Runnables:
  -  npm run eslint
  -  npm run coverage
  -  npm test

Usage:
node inviteSender.js [ARGS]

Mandatory arguments:
  -  --maxDistance={} Number
  -  --lon={} Number
  -  --lat={} Number
  -  --inputFile={} String (File needs to be JSON)

Example:
node inviteSender.js --maxDistance=30 --lat=51.839549 --lon=5.87837 --inputFile=./partners.json
