const { expect } = require('chai');
const inviteSender = require('../src/inviteSender');
const correctCompletePartners = require('./mocks/correct-complete-partners');

describe('InviteSender test', () => {
  it('With valid input, getPartnersToSendInviteTo, should return correct result', async () => {
    const appArgs = [
      '--maxDistance=75',
      '--lat=51.839549',
      '--lon=5.87837',
      '--inputFile=test/mocks/partners.json'];
    const result = await inviteSender.getPartnersToSendInviteTo(appArgs);
    expect(result).to.deep.equal(correctCompletePartners);
  });
});
