const { expect } = require('chai');
const retry = require('../../src/utils/retry');

describe('Retry tests', () => {
  it('with resolve function with arguments, retryWrapper, should return promise resolve', async () => {
    const fn = (a, b) => Promise.resolve({ a, b });
    const result = await retry.retryWrapper(fn, ['arg1', 'arg2']);
    expect(result).to.deep.equal({ a: 'arg1', b: 'arg2' });
  });

  it('with resolve function without arguments, retryWrapper, should return promise resolve', async () => {
    const fn = () => Promise.resolve(true);
    const result = await retry.retryWrapper(fn);
    expect(result).to.deep.equal(true);
  });

  it('with reject function without arguments, retryWrapper, should return promise reject', async () => {
    const fn = () => Promise.reject('rejected');
    try {
      await retry.retryWrapper(fn);
    } catch (err) {
      expect(err).to.equal('Max retries 3 exceeded. Function error: rejected');
    }
  });

  it('fn should reject twice then resolve, retryWrapper, should return promise resolve', async () => {
    let count = 0;
    const fn = () => {
      count += 1;
      if (count < 2) return Promise.reject();
      return Promise.resolve(true);
    };
    const result = await retry.retryWrapper(fn);
    expect(result).to.equal(true);
  });
});
