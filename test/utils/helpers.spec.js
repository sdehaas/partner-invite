const Path = require('path');
const { expect } = require('chai');
const helpers = require('../../src/utils/helpers');

describe('Helper tests', () => {
  it('no param given, ensureValidFilePath, should throw error', () => {
    expect(helpers.ensureValidFilePath.bind(null, null)).to.throw('--inputFile is required');
  });

  it('path without .json extension, ensureValidFilePath, should throw an error', () => {
    expect(helpers.ensureValidFilePath.bind(null, '/tmp/test.txt'))
      .to
      .throw('--inputFile needs to point to .json file');
  });

  it('valid path, ensureValidFilePath, should not throw an error', () => {
    helpers.ensureValidFilePath(Path.resolve('test/mocks/valid-test-json-file.json'));
  });

  it('No lat passed, ensureValidLat, should throw an error', () => {
    expect(helpers.ensureValidLat.bind(null, null)).to.throw('--lat is required and to be a number');
  });

  it('Invalid lat, ensureValidLat, should throw an error', () => {
    expect(helpers.ensureValidLat.bind(null, 'abc')).to.throw('--lat (abc) is not a number');
  });

  it('Invalid number, ensureValidLat, should throw an error', () => {
    expect(helpers.ensureValidLat.bind(null, parseFloat('abc'))).to.throw('--lat is required and to be a number');
  });

  it('Valid number, ensureValidLat, should not throw an error', () => {
    helpers.ensureValidLat(3.12);
  });

  it('Invalid lon, ensureValidLon, should throw an error', () => {
    expect(helpers.ensureValidLon.bind(null, 'abc')).to.throw('--lon (abc) is not a number');
  });

  it('Invalid number, ensureValidLon, should throw an error', () => {
    expect(helpers.ensureValidLon.bind(null, parseFloat('abc'))).to.throw('--lon is required and to be a number');
  });

  it('Valid number, ensureValidLon, should not throw an error', () => {
    helpers.ensureValidLon(3.12);
  });

  it('No maxDistance, ensureValidMaxDistance, should throw an error', () => {
    expect(helpers.ensureValidMaxDistance.bind(null, null)).to.throw('--maxDistance is required');
  });

  it('Valid maxDistance number, ensureValidMaxDistance, should not throw an error', () => {
    helpers.ensureValidMaxDistance(3);
  });

  it('Valid input parameters, readAppParameters, should return all input params', () => {
    const inputArgs = {
      maxDistance: 3,
      lon: 3.123,
      lat: 51.23,
      inputFilePath: '/tmp/test.json',
    };
    const input = [
      `--maxDistance=${inputArgs.maxDistance}`,
      `--lon=${inputArgs.lon}`,
      `--lat=${inputArgs.lat}`,
      `--inputFile=${inputArgs.inputFilePath}`];
    const result = helpers.readAppParameters(input);
    expect(result).to.have.property('maxDistance').to.equal(inputArgs.maxDistance);
    expect(result).to.have.property('lon').to.equal(inputArgs.lon);
    expect(result).to.have.property('lat').to.equal(inputArgs.lat);
    expect(result).to.have.property('inputFilePath').to.include('test.json');
  });

  it('Partial input parameters, readAppParameters, should return partial input params', () => {
    const inputArgs = {
      lon: 3.123,
      lat: 51.23,
    };
    const input = [`--lon=${inputArgs.lon}`, `--lat=${inputArgs.lat}`];
    const result = helpers.readAppParameters(input);
    expect(result).to.have.property('lon').to.equal(inputArgs.lon);
    expect(result).to.have.property('lat').to.equal(inputArgs.lat);
    expect(result).to.have.property('maxDistance').to.equal(null);
    expect(result).to.have.property('inputFilePath').to.equal(null);
  });

  it('Valid input parameters, loadAppArguments, should return all input params', () => {
    const inputArgs = {
      maxDistance: 3,
      lon: 3.123,
      lat: 51.23,
      inputFilePath: 'test/mocks/valid-test-json-file.json',
    };
    const input = [
      `--maxDistance=${inputArgs.maxDistance}`,
      `--lon=${inputArgs.lon}`,
      `--lat=${inputArgs.lat}`,
      `--inputFile=${inputArgs.inputFilePath}`];
    const result = helpers.loadAppArguments(input);
    expect(result).to.have.property('maxDistance').to.equal(inputArgs.maxDistance);
    expect(result).to.have.property('lon').to.equal(inputArgs.lon);
    expect(result).to.have.property('lat').to.equal(inputArgs.lat);
    expect(result).to.have.property('inputFilePath').to.include('valid-test-json-file.json');
  });
});
