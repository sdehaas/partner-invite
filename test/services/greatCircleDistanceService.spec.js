const { expect } = require('chai');
const greatCircleDistanceService = require('../../src/services/greatCircleDistanceService');

describe('Great Circle Distance tests', () => {
  it('with valid lon/lat, calculateGreatCircleDistance, should return correct distance', async () => {
    const distance = greatCircleDistanceService.calculateGreatCircleDistance(
      5.87837,
      51.839549,
      6.01947237075907,
      50.7551194,
    );
    expect(distance).to.equal(121);
  });

  it('with missing lon/lat, calculateGreatCircleDistance, should return NaN', async () => {
    const distance = greatCircleDistanceService.calculateGreatCircleDistance(5.87837, 51.839549, 6.01947237075907);
    expect(Number.isNaN(distance)).to.equal(true);
  });

  it('with missing lon/lat, calculateGreatCircleDistance, should return NaN', async () => {
    const distance = greatCircleDistanceService.calculateGreatCircleDistance(5.87837, 51.839549);
    expect(Number.isNaN(distance)).to.equal(true);
  });

  it('with missing lon/lat, calculateGreatCircleDistance, should return NaN', async () => {
    const distance = greatCircleDistanceService.calculateGreatCircleDistance();
    expect(Number.isNaN(distance)).to.equal(true);
  });
});
