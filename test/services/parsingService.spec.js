const { expect } = require('chai');
const Path = require('path');
const parsingService = require('../../src/services/parsingService');
const validTestJsonFile = require('../mocks/valid-test-json-file');

describe('Parsing Service tests', () => {
  it('with valid json file, loadJSONFromInputFile, should return correct json', async () => {
    const result = await parsingService.loadJSONFromInputFile(Path.resolve('./test/mocks/valid-test-json-file.json'));
    expect(result).to.deep.equal(validTestJsonFile);
  });

  it('with invalid json file, loadJSONFromInputFile, should return an error', async () => {
    try {
      await parsingService.loadJSONFromInputFile(Path.resolve('./test/mocks/invalid-json-file.json'));
    } catch (err) {
      expect(err.toString()).to.include('Unexpected token , in JSON');
    }
  });

  it('with invalid file path, loadJSONFromInputFile, should return an error', async () => {
    try {
      await parsingService.loadJSONFromInputFile(Path.resolve('./test/mocks/non-existing-file.json'));
    } catch (err) {
      expect(err.toString()).to.include('ENOENT');
    }
  });

  it('valid json, parseDataAsJson, should return valid json object', async () => {
    const testJson = { test: 'validjson' };
    const result = parsingService.parseDataAsJson(JSON.stringify(testJson));
    expect(result).to.deep.equal(testJson);
  });
});
