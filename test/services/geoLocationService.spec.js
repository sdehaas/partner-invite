const { expect } = require('chai');
const geoLocationService = require('../../src/services/geoLocationService');

describe('Geo Location Service tests', () => {
  it('with custom address info, getCoordinatesForAddress, should return correct lon/lat', async () => {
    const { lon, lat } = await geoLocationService.getCoordinatesForAddress(
      'Viergrenzenweg',
      '97',
      'Vaals',
      'The Netherlands',
    );
    expect(lon).to.equal(6.01947237075907);
    expect(lat).to.equal(50.7551194);
  });

  it('with incomplete address info, getCoordinatesForAddress, should return promise reject with error', async () => {
    let result = null;
    try {
      result = await geoLocationService.getCoordinatesForAddress('test', '97', 'Vaals');
    } catch (err) {
      expect(err).to.equal('Incomplete data: Street: test, StreetNumber: 97, City: Vaals Country: undefined');
    } finally {
      expect(result).to.equal(null);
    }
  });

  it('with incomplete address info, getCoordinatesForAddress, should return promise reject with error', async () => {
    let result = null;
    try {
      result = await geoLocationService.getCoordinatesForAddress('test', '97', null, 'Vaals');
    } catch (err) {
      expect(err).to.equal('Incomplete data: Street: test, StreetNumber: 97, City: null Country: Vaals');
    } finally {
      expect(result).to.equal(null);
    }
  });

  it('with incomplete address info, getCoordinatesForAddress, should return promise reject with error', async () => {
    let result = null;
    try {
      result = await geoLocationService.getCoordinatesForAddress();
    } catch (err) {
      expect(err)
        .to
        .equal('Incomplete data: Street: undefined, StreetNumber: undefined, City: undefined Country: undefined');
    } finally {
      expect(result).to.equal(null);
    }
  });

  it('with correct server response, getLonLatFromResult, should return lon/lat', async () => {
    const mockResponse = [{ lon: 1.123, lat: 3.321 }];
    const { lon, lat } = geoLocationService.getLonLatFromResult(mockResponse);
    expect(lon).to.equal(1.123);
    expect(lat).to.equal(3.321);
  });

  it('with null server response, getLonLatFromResult, should throw an no results found error', () => {
    expect(geoLocationService.getLonLatFromResult.bind(null, null)).to.throw('No result found');
  });

  it('with empty array server response, getLonLatFromResult, should throw an no results found error', () => {
    expect(geoLocationService.getLonLatFromResult.bind(null, [])).to.throw('No result found');
  });

  it('with empty object array server response, getLonLatFromResult, should throw an No lon/lat found in result error',
    () => {
      expect(geoLocationService.getLonLatFromResult.bind(null, [{}])).to.throw('No lon/lat found in result');
    });
});
