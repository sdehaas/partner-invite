const { expect } = require('chai');
const { stub } = require('sinon');
const geoLocationService = require('../../src/services/geoLocationService');
const partnerService = require('../../src/services/partnerService');
const unfilteredPartners = require('../mocks/unfilteredPartners');
const filteredPartnersMax55km = require('../mocks/filteredPartnersMax55km');
const filterdPartnersMax10KM = require('../mocks/filterdPartnersMax10KM');
const completedPartnerInfo = require('../mocks/completedPartnerInfo');
const unsortedPartners = require('../mocks/unsortedPartners');
const sortedPartners = require('../mocks/sortedPartners');

let getCoordinatesForAddressStub = null;

describe('Partner service tests', () => {

  before(() => {
    getCoordinatesForAddressStub = stub(geoLocationService, 'getCoordinatesForAddress').
        callsFake(() => Promise.resolve({ lon: 5.651, lat: 51.839 }));
  });

  after(() => {
    getCoordinatesForAddressStub.restore();
  });

  it('with unsorted partner array, sortPartnersByName, should return sorted partners by name field ASC', () => {
    const result = partnerService.sortPartnersByName(unsortedPartners);
    expect(result).to.deep.equal(sortedPartners);
  });

  it('with unfiltered partner array, filterPartnersBasedOnMaxDistance, should return filtered result for 55KM', () => {
    const result = partnerService.filterPartnersBasedOnMaxDistance(unfilteredPartners, 55);
    expect(result).to.deep.equal(filteredPartnersMax55km);
  });

  it('with unfiltered partner array, filterPartnersBasedOnMaxDistance, should return filtered result for 10KM', () => {
    const result = partnerService.filterPartnersBasedOnMaxDistance(unfilteredPartners, 10);
    expect(result).to.deep.equal(filterdPartnersMax10KM);
  });

  it('with unfiltered partner array, filterPartnersBasedOnMaxDistance, should return empty array for undefined KM',
      () => {
        const result = partnerService.filterPartnersBasedOnMaxDistance(unfilteredPartners);
        expect(result).to.deep.equal([]);
      });

  it('without partner address, completeDataForPartner, should throw error', async () => {
    try {
      await partnerService.completeDataForPartner({});
    } catch (err) {
      expect(err.toString()).to.includes('missing address Object');
    }
  });

  it('with partner object, completeDataForPartner, should return completed partner info ', async () => {
    const partner = {
      name: 'test',
      address: {
        street: 'teststreet',
        no: '123',
        city: 'testcity',
        country: 'testnation',
      },
    };
    const completedPartner = await partnerService.completeDataForPartner(partner, 5.87, 51.83);
    expect(completedPartner.address).to.have.property('lon').to.equal(5.651);
    expect(completedPartner.address).to.have.property('lat').to.equal(51.839);
    expect(completedPartner.address).to.have.property('street').to.equal(partner.address.street);
    expect(completedPartner.address).to.have.property('no').to.equal(partner.address.no);
    expect(completedPartner.address).to.have.property('city').to.equal(partner.address.city);
    expect(completedPartner.address).to.have.property('country').to.equal(partner.address.country);
    expect(completedPartner).to.have.property('name').to.equal(partner.name);
    expect(completedPartner).to.have.property('distance').to.equal(15.1);
  });

  it('without partner array, completePartnerInfo, should throw error', async () => {
    try {
      await partnerService.completePartnerInfo();
    } catch (err) {
      expect(err.toString()).to.includes('No array passed to completePartnerInfo function');
    }
  });

  it('without partner array but an object instead, completePartnerInfo, should throw error', async () => {
    try {
      await partnerService.completePartnerInfo({});
    } catch (err) {
      expect(err.toString()).to.includes('No array passed to completePartnerInfo function');
    }
  });

  it('with partner array but without lon/lat, completePartnerInfo, should throw error', async () => {
    try {
      await partnerService.completePartnerInfo([]);
    } catch (err) {
      expect(err.toString()).to.includes('Lon/Lat params are mandatory for completePartnerInfo function');
    }
  });

  it('with valid partner array, completePartnerInfo, should return completed partner array', async () => {
      const completedPartners = await partnerService.completePartnerInfo(unsortedPartners, 6.0, 52.3);
      expect(completedPartners).to.deep.equal(completedPartnerInfo);
  });
});
